use std::borrow::BorrowMut;
use std::os::windows::ffi::OsStrExt;
use std::ffi::OsStr;
use std::ffi::OsString;
use std::os::windows::ffi::OsStringExt;
use std::os::windows::process::CommandExt;
use std::process::Command;

fn bin() -> Command {
    Command::new(env!("CARGO_BIN_EXE_tester"))
}

#[derive(Default)]
struct Dump {
    args_os: Vec<OsString>,
    wild: Vec<OsString>,
    CommandLineToArgvW: Vec<OsString>,
    GetCommandLineW: Vec<OsString>,
}

struct Unquoted<'a>(&'a OsStr);
impl fmt::Debug for Unquoted<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use std::fmt::Write;
        f.write_char('<')?;
        for c in self.0.encode_wide() {
            if let Some(c) = char::from_u32(c as _).filter(|&c| !c.is_control() && (c == ' ' || !c.is_whitespace())) {
                f.write_char(c)?;
            } else {
                f.write_char('{')?; c.fmt(f)?; f.write_char('}')?;
            }
        }
        f.write_char('>')
    }
}

use std::fmt;
impl fmt::Debug for Dump {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fn unq(v: &[OsString]) -> Vec<Unquoted<'_>> {
            v.iter().map(|v| Unquoted(v.as_os_str())).collect()
        }
        f.debug_struct("Dump")
        .field("args_os", &unq(&self.args_os))
        .field("wild", &unq(&self.wild))
        .field("CommandLineToArgvW", &unq(&self.CommandLineToArgvW))
        .field("GetCommandLineW", &unq(&self.GetCommandLineW))
        .finish()
    }
}

#[track_caller]
fn bounce(mut cmd: impl BorrowMut<Command>) -> Dump {
    let out = cmd.borrow_mut().output().unwrap();
    assert!(out.status.success());
    let out = String::from_utf8(out.stdout).unwrap();
    let mut dump = Dump::default();
    let mut junk = Vec::new();
    let mut current = &mut junk;
    for line in out.lines() {
        let line = line.trim();
        match line {
            "" => continue,
            "args_os" => current = &mut dump.args_os,
            "wild" => current = &mut dump.wild,
            "GetCommandLineW" => current = &mut dump.GetCommandLineW,
            "CommandLineToArgvW" => current = &mut dump.CommandLineToArgvW,
            _ => {
                let wide_chars = line.split_whitespace().map(|v| v.parse().unwrap()).collect::<Vec<u16>>();
                let s = OsString::from_wide(&wide_chars);
                current.push(s);
            }
        }
    }
    assert!(junk.is_empty(), "{junk:#?}");
    dump
}

#[track_caller]
fn check(dump: Dump) {
    assert_eq!(dump.wild, dump.CommandLineToArgvW, "{dump:#?}");
    assert_eq!(dump.args_os, dump.wild, "{dump:#?}");
}

#[track_caller]
fn check_raw(cmdline: impl AsRef<OsStr>) {
    let mut bin = bin();
    bin.raw_arg(cmdline);
    check(bounce(bin));
}

#[test]
fn simple_cases() {
    let mut cmd = bin();
    cmd.arg("with space");
    cmd.arg(" ");
    cmd.arg("");
    cmd.arg(" space-around ");
    cmd.arg("single'apos");
    cmd.arg("'two'");
    cmd.arg("the'two'inside");
    cmd.arg("\"quoted\"");
    cmd.arg("a\"quoted\"inside");
    check(bounce(cmd));

    check_raw("hello \"world\"");
    check_raw("        hello       \"world\"       ");
    check_raw("   \t     hello  \t     \"world\" \t      ");
    check_raw("\t'\thi\t'\t");
}

#[test]
fn slashes() {
    let mut cmd = bin();
    cmd.arg("/");
    cmd.arg("//");
    cmd.arg("///");
    cmd.arg("'/'");
    cmd.arg("'//'");
    cmd.arg("\"//\"");
    cmd.arg("/\"//\"/");
    check(bounce(cmd));

    check_raw("/ // /// ////       /////      /");
}

#[test]
fn all_separators1() {
    for c in 1..500 {
        let raw = format!("cursed{}separator", char::from_u32(c).unwrap());
        check_raw(raw);
    }
}

#[test]
fn all_separators2() {
    for c in 500..1000 {
        let raw = format!("cursed{}separator", char::from_u32(c).unwrap());
        check_raw(raw);
    }
}

#[test]
fn all_separators3() {
    for c in '\u{0001}'..char::MAX {
        if !c.is_whitespace() && !c.is_control() {
            continue;
        }
        let raw = format!("{c}\\{c}{c}x{c}y");
        check_raw(raw);
    }
}

#[test]
fn unqoted_backslashes() {
    check_raw(r#"\ -f"#);
    check_raw(r#".\path\to\dir\\ -f"#);
    check_raw(r#".\path\to\dir\ -f"#);
    check_raw(r#" .\path\to\dir\ -f "#);
    check_raw(r#"'.\path\to\dir\' '-f'"#);
    check_raw(r#"\"#);
    check_raw(r#"\\"#);
    check_raw(r#"\\\"#);
    check_raw(r#"\\\\"#);
    check_raw(r#"\\\\\"#);
    check_raw(r#"\\\\\\"#);
    check_raw(r#"\ a"#);
    check_raw(r#"\\ a"#);
    check_raw(r#"\\\ a"#);
    check_raw(r#"\\\\ a"#);
    check_raw(r#"\\\\\ a"#);
    check_raw(r#"\\\\\\ a"#);
    check_raw(r#"\ "#);
    check_raw(r#"\ \"#);
    check_raw(r#"\ \\"#);
    check_raw(r#"\ \\\"#);
    check_raw(r#"\ \\\\"#);
    check_raw(r#"\ \\\\\"#);
}
