use std::ffi::OsStr;
use std::os::windows::ffi::OsStrExt;

fn main() {
    println!("args_os");
    for arg in std::env::args_os() {
        dump_osstr(&arg);
    }
    println!("wild");
    for arg in wild::args_os() {
        dump_osstr(&arg);
    }
    dump_cmdw();
}


fn dump_cmdw() {
    use winapi::um::shellapi::CommandLineToArgvW;
    unsafe {
        let command_line = GetCommandLineW();
        if !command_line.is_null() {
            println!("GetCommandLineW");
            dump_wide_cstr(command_line);

            let mut cnt = 0;
            let args = CommandLineToArgvW(command_line, &mut cnt);
            println!("CommandLineToArgvW");
            if !args.is_null() {
                for i in 0..cnt as usize {
                    dump_wide_cstr(*args.add(i));
                }
            }
            // going to leak - no LocalFree
        }
    }
}

#[track_caller]
unsafe fn dump_wide_cstr(mut s: *const u16) {
    assert!(!s.is_null());
    loop {
        let c = *s; s = s.add(1);
        if c == 0 { break; }
        print!("{c} ");
    }
    println!();
}


fn dump_osstr(s: &OsStr) {
    for c in s.encode_wide() {
        print!("{c} ");
    }
    println!();
}

extern "C" {
    pub fn GetCommandLineW() -> *mut u16;
}
